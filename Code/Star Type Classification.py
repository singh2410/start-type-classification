#!/usr/bin/env python
# coding: utf-8

# # Star Type Classification Model using KNN and Decision Tree
# #By- Aarush Kumar
# #Dated: Sept. 25,2021

# In[1]:


from IPython.display import Image
Image(url='https://c4.wallpaperflare.com/wallpaper/375/565/78/fantastic-universe-mz-space-stars-galaxies-ultra-hd-desktop-wallpaper-preview.jpg')


# In[2]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import time
import seaborn as sns
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score


# In[4]:


dfStarClassification = pd.read_csv("/home/aarush100616/Downloads/Projects/Star Type Classification/Data/Stars.csv",sep=",")
dfStarClassification.head()


# In[5]:


dfStarClassification.info()


# In[6]:


dfStarClassification = dfStarClassification.astype({"Type": str})


# In[7]:


dfStarClassification.info()


# In[8]:


dfStarClassification.Color.unique()


# In[9]:


def ColoumnArrangement(df,ColoumnName):
    dfSeries = pd.Series(data=df[ColoumnName])
    dfSeries = dfSeries.str.upper()
    dfSeries = dfSeries.str.replace(" ","-")
    df = df.drop(columns=[ColoumnName])
    df.insert(loc=4,column=ColoumnName,value= dfSeries)
    return df


# In[10]:


dfStarClassification = ColoumnArrangement(dfStarClassification,"Color")


# In[11]:


dfStarClassification.head()


# In[12]:


dfStarClassification.Color.unique()


# In[13]:


dfStarClassification.Spectral_Class.unique()


# In[14]:


correlation = dfStarClassification.corr()
correlation


# In[15]:


sns.heatmap(correlation,xticklabels=correlation.columns,yticklabels=correlation.columns)


# In[16]:


sns.catplot(x="Type",data= dfStarClassification,kind="count")
plt.show()


# In[17]:


continuousColumns = ["Temperature","L","R","A_M"]
for item  in continuousColumns:
    sns.boxplot(x="Type", y=item, data=dfStarClassification)
    plt.show()


# ## Preprocessing

# In[18]:


willOneHotEncode = ["Color","Spectral_Class"]
willScale  = continuousColumns


# In[19]:


for item in willOneHotEncode:
    dfStarClassification = pd.concat([dfStarClassification,pd.get_dummies(dfStarClassification[item],prefix=item)],axis=1)
    dfStarClassification = dfStarClassification.drop(columns=[item])
dfStarClassification.head()


# In[21]:


minMaxScaler = MinMaxScaler()
scaledColums = pd.DataFrame(minMaxScaler.fit_transform(dfStarClassification[willScale]),columns=willScale)
scaledColums.describe()


# In[22]:


dfStarClassification.drop(willScale,axis=1,inplace=True)


# In[23]:


dfStarClassification = pd.concat([dfStarClassification,scaledColums],axis=1)


# In[24]:


dfStarClassification


# In[25]:


correlation = dfStarClassification.corr().abs()
correlation.head()


# ## Train Test Split

# In[26]:


target = ["Type"]
features = dfStarClassification.columns.drop(target)
train,test = train_test_split(dfStarClassification,test_size = 0.22,random_state= 12)
xTrain = train[features]
yTrain = train[target]
xTest  = test[features]
yTest = test[target]


# ## KNN

# In[27]:


from sklearn.neighbors import KNeighborsClassifier


# In[28]:


knnModel = KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='minkowski',
                     metric_params=None, n_jobs=None, n_neighbors=6, p=2,
                     weights='uniform')

knnModel= KNeighborsClassifier().fit(xTrain,yTrain.values.ravel())


# In[29]:


yPred= knnModel.predict(xTest)
accuracy_score(yTest,yPred)


# In[30]:


print(classification_report(yTest,yPred))


# ## DecisionTree

# In[31]:


from sklearn.tree import DecisionTreeClassifier


# In[32]:


dTree = DecisionTreeClassifier(criterion="gini", max_depth=4)
start = time.time()
dTree.fit(xTrain, yTrain)
end = time.time()
preddt = dTree.predict(xTest)
print(classification_report(yTest,preddt))
print("Prosesing Time",end-start)


# In[33]:


dTree = DecisionTreeClassifier(criterion="entropy", max_depth=3)
start = time.time()
dTree.fit(xTrain, yTrain)
end = time.time()
preddt = dTree.predict(xTest)
print(classification_report(yTest,preddt))
print("Prosesing Time: ",end-start)

